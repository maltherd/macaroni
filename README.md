# macaroni
the most scuffed macarons image + email generator and sender you have ever seen.

## install
1. have [poetry](https://python-poetry.org/).
2. `cd` to the repo root folder
3. `poetry install`

## use
1. `cd` to the repo root folder
2. `poetry shell`
3. `python macaroni.py`

## email content
If you use the email generator, you can change the text by tinkering with the templates down in the `templates/` folder.

The templates use jinja2.

## testing the emails
Look at the `TEST_EMAIL_ADDRESS` and `SEND_EMAILS_TO_TEST_ADDRESS` settings.

Set the first one to your personal email address, and the latter to `True`.

## sending the emails
Obviously, don't do that before you're certain that the emails look good.

To activate the automatic email sending, you must edit the `macaroni.py` script and change the following variables:

1. `PRO_MAILBOX_PASSWORD` must be filled with the current pro mailbox password.
2. `SEND_EMAILS` must be set to `True`
3. You must go into the security settings of your Gmail pro account and set "Less secure app access" to ON. Otherwise macaroni can't send email for you.

Then, simply run the program like shown at the top. Careful! There is no going back after that.
