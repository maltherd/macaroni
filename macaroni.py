import os
import csv
import smtplib
from datetime import datetime
from collections import defaultdict
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from jinja2 import Environment, FileSystemLoader

from PIL import Image, ImageDraw, ImageFont


PRO_MAILBOX_PASSWORD = os.getenv("MACARONI_PRO_PASSWORD")
SEND_EMAILS = True
SEND_EMAILS_TO_TEST_ADDRESS = False
TEST_EMAIL_ADDRESS = "arnaud.gaudard@japan-impact.ch"
FONT_NAME = "font.ttf"
TEXT_COORDS = (250, 403)
TEXT_SPAN_LENGTH = 825
TEXT_INITIAL_SIZE = 70
COLOR_TO_PARKING_MAP = {
    "Rouge": "Piccard",
    "Vert": "Piccard",
    "Jaune": "Esplanade",
    "Violet": "Esplanade",
    "Bleu": "Rivier",
    "Orange": "Rivier",
}


def find_font(text, max_width, size):
    font = None
    while size > 1:
        font = ImageFont.truetype(FONT_NAME, size, encoding="unic")

        width, u = font.getsize(text)

        if width <= max_width:
            return font
        else:
            size = size - 1

    return font


def send_mail(email_to, title, mailtext, plan_file_paths, macaron_image_paths):
    msg = MIMEMultipart()
    msg["Subject"] = f"TEST - {title}" if SEND_EMAILS_TO_TEST_ADDRESS else title
    msg["From"] = "Responsable Pro <pro@japan-impact.ch>"
    msg["To"] = TEST_EMAIL_ADDRESS if SEND_EMAILS_TO_TEST_ADDRESS else email_to

    if not SEND_EMAILS_TO_TEST_ADDRESS:
        msg["Cc"] = "pro@japan-impact.ch"

    text = MIMEText(mailtext, "html")
    msg.attach(text)

    for image_path in macaron_image_paths:
        imgbin = open(image_path, "rb").read()
        image = MIMEImage(imgbin, "png", name=os.path.basename(image_path))
        msg.attach(image)

    for plan_path in plan_file_paths:
        planbin = open(plan_path, "rb").read()
        plan = MIMEImage(planbin, "png", name=os.path.basename(plan_path))
        msg.attach(plan)

    s = smtplib.SMTP("smtp.gmail.com", 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login("pro@japan-impact.ch", PRO_MAILBOX_PASSWORD)

    if SEND_EMAILS_TO_TEST_ADDRESS:
        s.sendmail(msg["From"], [msg["To"]], msg.as_string())
    else:
        s.sendmail(msg["From"], [msg["To"], msg["Cc"]], msg.as_string())

    s.quit()

    print("Mail sent to {}".format(msg["To"]))


def process_one_macaron(text, color):
    # Create the macaron image.
    img = Image.open("templates/macaron2022{}.png".format(color.lower()))
    draw = ImageDraw.Draw(img)

    text = text.replace("\n", "").upper()
    draw.text(
        TEXT_COORDS,
        text,
        (0, 0, 0),
        font=find_font(text, TEXT_SPAN_LENGTH, TEXT_INITIAL_SIZE),
    )

    img_file = "results/images/{}.png".format(text.replace("/", "_"))
    img.save(img_file)

    return img_file


def process_contact(email, tel, english, parkings, colors, macaron_image_files):
    main_template_file = "templates/mail.html.jinja"
    plan_files = [f"plans/parking_{parking}.png" for parking in parkings]

    mail_template = Environment(loader=FileSystemLoader("templates")).from_string(
        open(main_template_file, "r").read()
    )
    mail_content = mail_template.render(
        pro_tel=tel,
        english=english,
        parkings=parkings,
        colors=colors,
    )

    title = "Mail d'accès et informations"
    mail_file = "results/mails/{}.html".format(email)
    with open(mail_file, "w") as f:
        f.write(mail_content)

    if SEND_EMAILS:
        send_mail(email, title, mail_content, plan_files, macaron_image_files)


def parse_row(row):
    prestataire = row[0]
    prenom = row[1]
    nom = row[2]
    plaque = row[3]
    couleur = row[4]
    email = row[5]
    language = row[6]
    pro_contact_number = row[7]
    email_already_sent = row[8] == "TRUE"

    if (
        not prestataire
        or not prenom
        or not nom
        or not plaque
        or "?" in plaque
        or not couleur
        or not pro_contact_number
    ):
        return None, None

    return prestataire, (
        f"{prestataire}/{prenom} {nom.upper()}/{plaque}",
        couleur,
        COLOR_TO_PARKING_MAP.get(couleur),
        email,
        language,
        pro_contact_number,
        email_already_sent
    )


grouped_data = defaultdict(list)
disqualified_groups = set()

with open("contacts.csv") as mac:
    reader = csv.reader(mac, delimiter=";", quotechar='"')

    for rownum, row in enumerate(reader):
        key, data = parse_row(row)

        if not data:
            print(f"Bad format => \tIgnoring  line {rownum + 1} : `{list(row)}`")

            if key in grouped_data:
                print(f"              \tAlso disqualifying the group `{key}`")
                disqualified_groups.add(key)

            continue

        if data[6]:
            print(f"Already sent => Ignoring  line {rownum + 1} : `{list(row)}`")
            continue

        if key in disqualified_groups:
            print(f"Group disqualified => Ignoring  line {rownum + 1} : `{list(row)}`")
            continue

        print(f"Okay => \tAccepting line {rownum + 1} : `{list(row)}`")

        grouped_data[key].append(data)

output_file_path = f"emails_sent_{datetime.now().strftime('%d_%m_%Y__%H_%M_%S')}.csv" if SEND_EMAILS else f"FAKE_emails_sent_{datetime.now().strftime('%d_%m_%Y__%H_%M_%S')}.csv"
with open(output_file_path, "w") as output_file:
    writer = csv.writer(output_file, delimiter=";", quotechar='"')

    for key, ls in grouped_data.items():

        email = None
        tel = None
        lang = None

        parkings = set()
        colors = set()
        macaron_image_files = []

        for m_text, m_color, m_parking, m_email, m_lang, m_tel, m_email_already_sent in ls:
            parkings.add(m_parking)
            colors.add(m_color)
            email = email or m_email
            tel = tel or m_tel
            lang = lang or m_lang
            macaron_image_files.append(process_one_macaron(m_text, m_color))

        process_contact(
            email, tel, lang == "Anglais", parkings, colors, macaron_image_files
        )

        writer.writerow([email, datetime.now().strftime("%A %d %b  %H:%M:%S")])

print("All done! Check the results/ folder.")
